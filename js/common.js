$(document).ready(function(){
  $(".navbtn").click(function(){
    $(".navbox").css("transform","translateX(0)");
  });
  $(".navbox .close").click(function(){
  	$(".navbox").css("transform","translateX(450px)");
  });
  $(".footer .weibo").hover(function(){
    $(".contact .img1").fadeToggle();
  });
  $(".footer .weixin").hover(function(){
    $(".contact .img2").fadeToggle();
  });
  $("#page4 .content li h3").click(function(){
  	$("#page4 .content li h3").removeClass('active')
  	$("#page4 .content li .detail").slideUp()
  	$(this).addClass('active')
  	$(this).next('.detail').slideDown()
  	$(this).next('.detail').addClass('active')
  })
});